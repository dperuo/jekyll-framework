jekyll-framework
================

Forked from [awolfe76/jekyll-framework][fork].

Includes Bootstrap v3.0 via CDN.

[fork]: https://github.com/awolfe76/jekyll-framework
